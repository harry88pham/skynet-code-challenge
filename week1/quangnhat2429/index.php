<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <!-- <script src="js/jquery-2.2.4.min.js"></script> -->
    </head>
    <body>
        <div class="container">
            <div id="pattern" style="display: none;" data-val="<?php require 'lib/php/utils.php'; ?>"></div>
            <form id='test' class="" action="" method="post">
                <textarea name="input" rows="8" cols="80"></textarea>
                <button type="submit" name="Submit" onclick="GetAddress(event)"value="Get Address">Get Address</button>
            </form>
            <hr>
            <ul class="results">
                <li>Ket qua se hien o day!</li>
            </ul>
        </div>
    </body>
    <style media="screen">
        body, html{
            width: 100%;
            height: 100%;
        }
        .container{
            width: 80%;
            margin: 0 auto;
            height: 100%;
        }
        ul.results{
            width: 100%;
            height: 45%;
            overflow: auto;
        }
    </style>
    <script type="text/javascript">
        var arrExp = ['PHÒNG', 'ĐỘI', 'TẦNG', 'KHU'];
        // var kq;
        function CutAtWords(str, word){
            return str.substring(str.indexOf(word));
        }

        function ShowResults(arr){
            for(var i=0; i< arr.length; i++){
                $('ul.results').append('<li>' + arr[i] + '</li>');
            }
        }

        function GetAddress(e){
            $('ul.results').html('');
            e.preventDefault();
            var pattern = new RegExp($('#pattern').attr('data-val'), 'g');
            var str = $('textarea[name=input]').val().toUpperCase();
            var r = str.match(pattern);
            var check = 0;

            if(r == null){
                ShowResults(['Không tìm thấy']);
                return;
            }
            for(var i=0; i< r.length; i++){
                // console.log(i);
                // check = 0;
                for(var j=0; j< arrExp.length; j++){
                    if(r[i].indexOf(arrExp[j])){
                        // console.log(CutAtWords(r[i], arrExp[j]));

                        r[i] = CutAtWords(r[i], arrExp[j]);
                        // console.log(r[i]);
                        check = 1;
                        break;
                    }
                }
                // if(check == 0){
                    // console.log('asdfsadf');
                    r[i] = r[i].match(/([A-Z]*[\d]+|[\d]+[\/\d]*[\S]*).*/g);

                    console.log(r[i][0]);
                    // continue;
                // }
            }
            ShowResults(r);
        }
    </script>
</html>
