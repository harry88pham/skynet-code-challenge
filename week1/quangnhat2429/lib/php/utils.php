<?php
require 'head.php';
require 'mid.php';
require 'termin.php';
$results ='';

// build parten has any of item in array
function anyOfArray($arr){
    $results="";
    foreach($arr as $key=>$item){
        // echo $head.length;
        $results .= $item;
        if($key < count($arr) -1)
            $results .= '|';
    }
    return $results;
}

// build array of acronym by array of string
function buildArrayAcronym($arr){
    // print_r($arr);
    // echo "<br>";
    $arrResults;
    foreach($arr as $key=>$item){
        // echo $item . ' - ' . getFirstCharofWord($item). "<br>";
        $arrResults[$key] = getFirstCharofWord($item);
    }
    return $arrResults;
}

// get acronym of sentence
function getFirstCharofWord($str){
    mb_internal_encoding("UTF-8");
    $words = explode(" ", $str);    // echo print_r($words);
    $acronym = "";
    // print_r($words);
    // echo "<br>";
    foreach ($words as $w) {
        // print_r(mb_substr($w, 0, 1));
        // echo "<br>";
    //   $acronym .= mb_convert_encoding($w[0], 'UTF-8');
        $acronym .= mb_substr($w, 0, 1);
    }
    return str_replace('-', '', $acronym);
}

// function to uppercase all words of UTF-8
function strUpperCase($str){
    return mb_strtoupper($str, 'UTF-8');
}

// build pattern regex
function buildRegexPattern(){
    require 'head.php';
    require 'mid.php';
    require 'termin.php';
    require 'exception.php';
    $results ="";

    // buill head of pattern
    $results .= '(';
    $results .= '('
            .   strUpperCase(anyOfArray($head))
            .  ')|('
            .   anyOfArray($headPattern)
            .   ')';
    $results .= ')';

    // add mid of pattern
    $results .= ".{0,100}";

    // add termination
    $results .= '('
            .   strUpperCase(anyOfArray($province))
            . '|' . strUpperCase(anyOfArray(buildArrayAcronym($province)))
            .')';

    // echo anyOfArray($province);
    foreach($exception as $ex){
        $results = str_replace("|$ex", "", $results);
    }
    return $results;
}
// print_r($province);
// echo "<br>";
// print_r(buildArrayAcronym($province));
// echo anyOfArray($province);
// echo "<br>";
// $TEST = 'VÀO NGÀY 11/10/2017 LÚC 17:09:60 NHỚ GHÉ PHÒNG 123, LẦU 3, KHU A, TÒA NHÀ WINMAX, ĐƯỜNG NGÔ QUYỀN, PHƯỜNG 12, QUẬN 3, TP.HCM LẤY ĐỒ ĂN NHE. Giao hàng tại  P301 F3 57 CAO THẮNG, Q3, TP.HCM HOẶC 123/3 ĐƯỜNG LÊ LỢI, PHƯỜNG BẾN NGHÉ, QUẬN 1, THÀNH PHỐ HỒ CHÍ MINH DDFG 123/3/3/4/10 ĐƯỜNG LÊ LỢI, PHƯỜNG BẾN NGHÉ, QUẬN 1, THÀNH PHỐ HỒ CHÍ MINH 123/5B ĐƯỜNG LÊ LỢI, PHƯỜNG 6, THÀNH PHỐ TUY HÒA, TỈNH PHÚ YÊN tap lap gì dây  lần ĐỘI 3 THÔN PHÚ LỢI, XÃ HÒA AN, HUYỆN PHÚ HÒA, TỈNH PHÚ YÊN  ĐỊNH DẠNG PHỔ BIẾN NHẤT NHƯ SAU: ';

$parten = buildRegexPattern();
echo $parten;


 ?>
